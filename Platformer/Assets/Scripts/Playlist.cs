﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist : MonoBehaviour
{
    [SerializeField]
    private AudioClip die;

    public void Die()
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.clip = die;
        audioSource.Play();
    }
	
}
