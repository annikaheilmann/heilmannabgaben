﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillPlayer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player == null)
        {
            return;
        }

        if (player.state == PlayerState.Dead)
        {
            return;
        }

        player.Kill();

        SceneManager.LoadScene("StartMenü");
    }
}