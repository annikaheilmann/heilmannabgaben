﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEnemy : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D impact)
    {
        if (impact.CompareTag("Enemy"))
        {
            Destroy(impact.gameObject);
            Destroy(gameObject);
        }
    }
}
