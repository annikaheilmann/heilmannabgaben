﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CometSpawner : MonoBehaviour
{
    public GameObject comet;
    float randomX;
    Vector2 spawnLocation;
    public float spawnRate = 2f;
    float nextSpawn = 0.0f;

    void Start()
    {

    }

    void Update()
    {
        if(Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randomX = Random.Range(-8.4f, 8.4f);
            spawnLocation = new Vector2(randomX, transform.position.y);
            Instantiate(comet, spawnLocation, Quaternion.identity);
        }
    }
}
