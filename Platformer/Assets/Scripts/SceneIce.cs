﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneIce : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D ice)
    {
        if (ice.CompareTag("Player"))
        {
            SceneManager.LoadScene("SceneIce");
        }
    }
}
