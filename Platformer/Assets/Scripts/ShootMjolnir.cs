﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMjolnir : MonoBehaviour
{
    public Transform mjolnirSpawn;

    //public float velocityX = 5f;
    //float velocityY = 0f;
    public Rigidbody2D mjolnir;
    Rigidbody2D mjolnirClone;

    public float mjolnirSpeed = 600f;

    public void Start()
    {
        mjolnirSpawn = GameObject.Find("MjolnirSpawn").transform;

        mjolnirClone = GetComponent<Rigidbody2D>();    
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            mjolnirClone = Instantiate(mjolnir, mjolnirSpawn.position, mjolnirSpawn.rotation);
            mjolnirClone.AddForce(mjolnirSpawn.transform.right * mjolnirSpeed);
            //mjolnirClone.velocity = new Vector2(velocityX, velocityY);
            Destroy(gameObject, 1f);
        }
    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Enemy"))
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }

    //public GameObject projectile;
    //public Vector2 velocity;
    //bool canShoot = true;
    //public Vector2 offset = new Vector2(0.4f, 0.1f);
    //public float cooldown = 1f;

    //void Start()
    //{

    //}

    //void Update()
    //{
    //    if(Input.GetKeyDown(KeyCode.E) && canShoot)
    //    {
    //        GameObject go = (GameObject)
    //        Instantiate(projectile,(Vector2)transform.position + offset * transform.localScale.x, Quaternion.identity);
    //        go.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x * transform.localScale.x, velocity.y);
    //        Destroy(projectile, 3f);
    //        StartCoroutine(CanShoot());
            
    //    }
    //}

    //IEnumerator CanShoot()
    //{
    //    canShoot = false;
    //    yield return new WaitForSeconds(cooldown);
    //    canShoot = true;
    //}
}
