﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour
{
    [SerializeField]
    private Vector3 offset = Vector3.back * 10;

    private Player target;

    private void Awake()
    {
        {
            target = FindObjectOfType<Player>();
        }
    }

    void Update()
    {
        gameObject.transform.position = target.transform.position + offset;
    }
}
