﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHeart : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.name != "Player")
        {
            return;
        }
        Destroy(this.transform.parent.gameObject);
    }

}
