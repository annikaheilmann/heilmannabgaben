﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Vector2 direction = Vector2.left;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();

        if ( player == null)
        {
            return;
        }

        this.enabled = false;
        player.Die();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        var ray = Physics2D.Raycast(transform.position, Vector2.down + direction, 2f, LayerMask.GetMask("Ground"));

        Debug.DrawRay(transform.position, Vector2.down + direction, Color.red);

        if (ray.transform == null)
        {
            direction = direction * -1;
        }
        else
        {
            var position = transform.position;
            position.x += direction.x * 2f * Time.fixedDeltaTime;
            transform.position = position;
        }

        var newRotation = gameObject.transform.localRotation.eulerAngles;
        if(direction == Vector2.left)
        {
            newRotation.y = 0f;
        }
        else if(direction == Vector2.right)
        {
            newRotation.y = 180f;
        }
        gameObject.transform.rotation = Quaternion.Euler(newRotation); 
    }

    
}
